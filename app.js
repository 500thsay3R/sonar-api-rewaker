/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint new-cap: 0 */
'use strict';

const bodyParser = require('body-parser');
const express = require('express');
const request = require('request');
const winston = require('winston');

const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: 'debug',
    }),
  ],
});

const router = express.Router();
const app = express();

app.set('port', process.env.PORT || '3000');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', router);


router.get('/reboot', (req, res) => {
  const name = req.query.name;
  logger.info(`[${new Date().toString()}] :: Reboot request received on ${name.toUpperCase()}.`);

  const options = {
    url: `https://api.heroku.com/apps/${name}/dynos/web.1`,
    method: 'DELETE',
    headers: {
      Accept: 'application/vnd.heroku+json; version=3',
      Authorization: `Bearer ${process.env.CREDENTIALS}`,
    },
  };

  request(options, (error, response) => {
    if (response && response.statusCode === 202) {
      logger.info(
        `[${new Date().toString()}] :: ${name.toUpperCase()} has been successfully restarted.`
      );

      res.status(200).json({ status: 'DONE' });
    } else if (error) {
      logger.error(
        `[${new Date().toString()}] :: Failed to reboot ${name.toUpperCase()} -> ${error.message}`
      );

      res.status(410).json({ status: 'FAILED' });
    } else {
      logger.info(
        `[${new Date().toString()}] :: Failed to reboot ${name.toUpperCase()} -> Wrong scenario.`
      );

      res.status(500).json({ status: 'FAILED' });
    }
  });
});

app.listen(app.get('port'), () => {
  logger.info(`SONAR API REWAKER is up & running on port #${app.get('port')}`);
});

module.exports = app;
